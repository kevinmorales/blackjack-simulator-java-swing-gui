import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/***************************************************************************
 *Title: 	   GameWindow
 *Author: 	   Kevin Morales
 *Date Due:    15 May 2019
 *Course: 	   CSC 264-501
 *Description: This class handles all the graphical elements of the program.
 *			   This class is the point of interaction for the user.
 *
 ***************************************************************************/
public class GameWindow extends JPanel
{
	//class constants
	final int INITIAL_PURSE = 1000;			//Initial money for player
	final ImageIcon UNKNOWN =
	new ImageIcon("CardImages//question2.png"); 		//Temporary icon for hidden cards

	//class variables

	/**** Integers *****/
	int totalMoney = INITIAL_PURSE;			//Total money for player
	int betAmount;							//Bet amount
	int totalBet = 0;						//Total bet
	int playerTotal = 0;					//Total of player's hand
	int dealerTotal = 0;					//Total of dealer's hand
	int revealCount = 1;					//Number of card's revealed by the dealer

	/**** Boolean flags *******/
	boolean aceDetected = false;			//Flag for detecting an ace in player hand
	boolean aceDetectedDealer = false;		//Flag for detecting an ace in dealer hand
	boolean stayDetected = false;			//Flag for detecting if a player stayed
	boolean openHelp = false;

	/****** Buttons **********/
	JButton bet50;							//Button to bet $50
	JButton bet100;							//Button to bet $100
	JButton bet500;							//Button to bet $500
	JButton bet1000;						//Button to bet $1000
	JButton betAll;							//Button to bet all money
	JButton quit;							//Button to quit game
	JButton readyForNext;					//Button to get a new hand of cards
	JButton playAgain;						//Button to start new game
	JButton stay;							//Button to stay
	JButton dealerCard;						//Button to hold dealer's card
	JButton playerCard;						//Button to hold player's card
	JButton help;							//Button to get help
	JButton [] bettingButtons;				//Array to hold betting buttons
	JButton [] dealerCardArray;				//Array to hold dealer's cards
	JButton [] playerCardArray;				//Array to hold player's cards

	/******** Labels and Panels *********/

	JLabel instruction1 = new JLabel("");	//Label for instruction 1
	JLabel instruction2 = new JLabel("");	//Label for instruction 2
	JLabel instruction3 = new JLabel("");	//Label for instruction 3
	JLabel betLabel;						//Label for current bet total
	JLabel score;							//Label for current score
	JLabel myMoney =
	new JLabel("My Money: $" + totalMoney);	//Label for total money
	JLabel endLabel =
	new JLabel("THANKS FOR PLAYING");		//Label for end of game message

	DecisionPanel myDecision;				//Panel for decision buttons
	GamePanel myGame;						//Panel that holds game
	EndPanel endGame;						//Panel for end of game
	ImageIcon cardImage;					//Icon for cards
	JFrame frame2;


	DeckOfCards deck;						//Deck of cards object
	Card myCard;							//Card object

	/****************************************************************
	*Title: 	   GameWindow
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This constructor instantiates all the components of the game
	*			   and adds them to the border layout.
	*
	*Refined Algorithm:
	*		BEGIN Game Window
	*			Set size of window
	*			Give user some money to start game
	*			Set up money label
	*			Set instance deck to class deck
	*			Set up Panel in Border Layout style
	*			Set up help window
	*			Instantiate new Game Panel
	*			Instantiate new End Panel
	*			Instantiate new Decision Panel
	*			Add money label
	*			Add decision panel
	*			Add end panel
	*			Add game panel
	*		END GameWindow
	*
	******************************************************************/

	public GameWindow(DeckOfCards inDeck)
	{
		//local constants

		//local variables

		/****************************/

		//Set size of window
		setPreferredSize(new Dimension(1300,900));

		//Give user some money to start game
		totalMoney = 1000;

		//Set up money label
		myMoney.setFont(new Font("Serif", Font.PLAIN, 40));

		//Set instance deck to class deck
		deck = inDeck;

		//Shuffle deck
		deck.shuffle();

		//Set up Panel in Border Layout style
		setLayout(new BorderLayout());

		//Instantiate new Game Panel
		myGame = new GamePanel();

		//Instantiate new End Panel
		endGame = new EndPanel();
		endGame.setVisible(false);

		//Instantiate new Decision Panel
		myDecision = new DecisionPanel();

		//Add money label
		add(myMoney, BorderLayout.NORTH);

		//Add decision panel
		add(myDecision, BorderLayout.EAST);

		//Add end panel
		add(endGame,BorderLayout.CENTER);

		//Add game panel
		add(myGame, BorderLayout.CENTER);


	}//END GameWindow constructor

	private class DecisionPanel extends JPanel
	{
		//class constants

		//class variables


		/****************************************************************
		*Title: 	   DecisionPanel
		*Author: 	   Kevin Morales
		*Date Due:     15 May 2019
		*Course: 	   CSC 264-501
		*Description:  This class handles all betting elements of the game.
		*
		*Refined Algorithm:
		*BEGIN DecisionPanel
		*	Set background to green
		*	Create bet label
		*	Create betting buttons
		*	Create stay button
		*	Create help button
		*	Put betting buttons in array
		*	Arrange buttons in vertical row
		*	Add the label for the buttons
		*	FOR each item in the array
		*		Give each button an actionListener
		*		Add each button to the panel
		*	END FOR
		*	Add stay button to panel
		*	Add help button to panel
		*END DecisionPanel
		*
		********************************************/

		public DecisionPanel()
		{
			//local constants

			//local variables

			/***************************/

			//Set background to green
			setBackground(Color.white);

			//Create bet label
			betLabel = new JLabel("BET: $0");
			betLabel.setFont(new Font("Serif", Font.PLAIN, 40));

			//Create betting buttons
			betAll = new JButton("ALL IN ");
			bet1000 = new JButton("$1,000   ");
			bet500 = new JButton("$500      ");
			bet100 = new JButton("$100      ");
			bet50 = new JButton("$50        ");

			//Create stay button
			stay = new JButton("STAY    ");
			stay.addActionListener(new ButtonListener());
			stay.setFont(new Font("Serif", Font.PLAIN, 40));

			//Create help button
			help = new JButton("HELP    ");
			help.addActionListener(new ButtonListener());
			help.setFont(new Font("Serif", Font.PLAIN, 40));

			//Put betting buttons in array
			bettingButtons = new JButton[]{bet50, bet100, bet500, bet1000, betAll};

			//Arrange buttons in vertical row
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

			//Add the label for the buttons
			add(betLabel);

			//FOR each item in the array
			for (int i = 0; i < bettingButtons.length; i++)
			{
				//Give each button an actionListener
				bettingButtons[i].addActionListener(new ButtonListener());

				bettingButtons[i].setFont(new Font("Serif", Font.PLAIN, 40));

				//Add each button to the panel
				add(bettingButtons[i]);

			}//END FOR

			//Add stay button to panel
			add(stay);

			//Add help button to panel
			add(help);

		}//END constructor

		private class ButtonListener implements ActionListener
		{

		/****************************************************************
		*Title: 	   DecisionPanel
		*Author: 	   Kevin Morales
		*Date Due:     15 May 2019
		*Course: 	   CSC 264-501
		*Description:  This class handles the consequences of pushing a betting button.
		*
		*Refined Algorithm:
		*BEGIN ButtonListener
		*		IF user stays
		*			set stay boolean flag to true
		*			FOR all elements in the array
		*			END FOR
		*			Disable stay button
		*			FOR each element in the array
		*				disable each button
		*			END FOR
		*			WHILE dealer total is less than 18
		*				Reveal a card
		*			END WHILE
		*		ELSE IF user pushes help
		*			Set up instructions
		*			Pop window up
		*		ELSE user did not stay
		*			IF user bets $50
		*				Set bet amount
		*			ELSE IF user bets $100
		*				Set bet amount
		*			ELSE IF user bets $500
		*				Set bet amount
		*			ELSE IF user bets $1,000
		*				Set bet amount
		*			ELSE IF user is all in
		*				set bet amount to total money
		*				FOR each button in the array
		*					Disable all buttons
		*				END FOR
		*			For each element in the array
		*				Enable cards
		*			END FOR
		*			Enable stay button
		*			Decrease total money by bet amount
		*			Increase total bet by bet amount
		*			IF user has no money left
		*				FOR each element in the array except the last one
		*					Disable that button
		*				END FOR
		*			IF less than $50 remains
		*				FOR each element in the array except the last one
		*					Disable that button
		*				END FOR
		*			ELSE IF less than $100 remains
		*				FOR each element in the array except the last one and the first one
		*					Disable that button
		*				END FOR
		*			ELSE IF less than $500 remains
		*				FOR each element in the array except for the first two and last one
		*					Disable that button
		*				END FOR
		*			ELSE IF less than $1000 remains
		*				Disable the $1000 button
		*			END IF
		*		END IF
		*	END ButtonListener
		*
		****************************************************/

			public void actionPerformed(ActionEvent event)
			{
				//local constants
				final String ZERO = "You start the game with 2 upcards and 4 hidden cards.\n";
				final String ONE = "1. You must first place a bet to pick a card.\n";
				final String TWO = "2. You pick any of your '?' cards.\n";
				final String THREE = "3. You can choose any of your cards, simply click on them.\n";
				final String FOUR = "4. To stay means to not draw any more cards after betting.\n";
				final String FIVE = "5. You can 'stay' when you think you can beat the dealer with your cards.\n";
				final String SIX = "6. If you run out of money while betting, you can no longer select cards.";

				//local variables
				Object source = event.getSource();
				String instructions = ZERO + ONE + TWO + THREE + FOUR + FIVE;

				/**************************/

				//IF user stays
				if (source == stay)
				{
					//set stay boolean flag to true
					stayDetected = true;

					//FOR all elements in the array
					for(int i = 0; i < bettingButtons.length; i++)

						bettingButtons[i].setEnabled(false);

					//END FOR

					//Disable stay button
					stay.setEnabled(false);

					//FOR each element in the array
					for (int i = 0; i < playerCardArray.length; i++)

						//disable each button
						playerCardArray[i].setEnabled(false);

					//END FOR

					//WHILE dealer total is less than 18
					while(dealerTotal < 18)

						//Reveal a card
						revealCard();

					//END WHILE

				}

				//ELSE IF user pushes help
				else if(source == help)
				{
					//Set up instructions
					JOptionPane.showMessageDialog(null, instructions);

				}

				//ELSE user did not stay
				else
				{

					//IF user bets $50
					if (source == bet50)

						//Set bet amount
						betAmount = 50;

					//ELSE IF user bets $100
					else if (source == bet100)

						//Set bet amount
						betAmount = 100;

					//ELSE IF user bets $500
					else if (source == bet500)

						//Set bet amount
						betAmount = 500;

					//ELSE IF user bets $1,000
					else if (source == bet1000)

						//Set bet amount
						betAmount = 1000;

					//ELSE IF user is all in
					else if (source == betAll)
					{
						//set bet amount to total money
						betAmount = totalMoney;

						//FOR each button in the array
						for (int i = 0; i < bettingButtons.length; i++)

							//Disable all buttons
							bettingButtons[i].setEnabled(false);

						//END FOR

					}

					//For each element in the array
					for (int i = 0; i < playerCardArray.length; i++)

						//Enable cards
						playerCardArray[i].setEnabled(true);

					//END FOR

					//Enable stay button
					stay.setEnabled(true);

					//Decrease total money by bet amount
					totalMoney -= betAmount;
					myMoney.setText("My Money: " + totalMoney);

					//Increase total bet by bet amount
					totalBet += betAmount;
					betLabel.setText("BET: $" + totalBet);

					//IF user has no money left
					if (totalMoney == 0)
					{

						//FOR each element in the array except the last one
						for(int i = 0; i < bettingButtons.length; i++)

							//Disable that button
							bettingButtons[i].setEnabled(false);

						//END FOR

					}

					//IF less than $50 remains
					else if (totalMoney < 50)

						//FOR each element in the array except the last one
						for(int i = 0; i < bettingButtons.length - 1; i++)

							//Disable that button
							bettingButtons[i].setEnabled(false);

						//END FOR

					//ELSE IF less than $100 remains
					else if(totalMoney < 100)

						//FOR each element in the array except the last one and the first one
						for(int i = 1; i < bettingButtons.length - 1; i++)

							//Disable that button
							bettingButtons[i].setEnabled(false);

						//END FOR

					//ELSE IF less than $500 remains
					else if (totalMoney < 500)

						//FOR each element in the array except for the first two and last one
						for(int i = 2; i < bettingButtons.length - 1; i++)

							//Disable that button
							bettingButtons[i].setEnabled(false);

						//END FOR

					//ELSE IF less than $1000 remains
					else if (totalMoney < 1000)

						//Disable the $1000 button
						bet1000.setEnabled(false);

					//END IF

				}//END IF

			}//END actionPerformed

		}//END ButtonListener

	}//END DecisionPanel

	private class GamePanel extends JPanel
	{
		//class constants

		//class variables
		DealerPanel dealerCards;	//Declaration of dealer panel
		PlayerPanel playerCards;	//Declaration of player panel
		CenterPanel center;			//Declaration of center panel

		/****************************************************************
		*Title: 	   GamePanel
		*Author: 	   Kevin Morales
		*Date Due:     15 May 2019
		*Course: 	   CSC 264-501
		*Description:  This class holds the actual card portion of the game
		*
		*Refined Algorithm:
		*BEGIN GamePanel
		*	Set up Panel in Border Layout style
		*	Instantiate Dealer Panel
		*	Instantiate Player Panel
		*	Instantiate CenterPanel
		*	Add the dealer panel, player panel and center panel
		*	Set background to green
		*END GamePanel
		*
		***********************************************************/

		public GamePanel()
		{
			//local constants

			//local variables

			/*******************/

			//Set up Panel in Border Layout style
			setLayout(new BorderLayout());

			//Instantiate Dealer Panel
			dealerCards = new DealerPanel();

			//Instantiate Player Panel
			playerCards = new PlayerPanel();

			//Instantiate CenterPanel
			center = new CenterPanel();

			//Add the dealer panel, player panel and center panel
			add(dealerCards, BorderLayout.NORTH);
			add(playerCards, BorderLayout.SOUTH);
			add(center, BorderLayout.CENTER);

			//Set background to green
			setBackground(Color.green);

		}//END GamePanel constructor

		private class DealerPanel extends JPanel
		{
			/*********************************************
			*Title: 	   DealerPanel
			*Author: 	   Kevin Morales
			*Date Due:     15 May 2019
			*Course: 	   CSC 264-501
			*Description:  This class deals with the dealer's actions during the game
			*
			*Refined Algorithm:
			*BEGIN GamePanel
			*	Set background to green
			*	Create 6 buttons that represent cards
			*		IF the first card is being created
			*			Create button
			*			Add to panel
			*			Get a card from the deck
			*			Get current card's image
			*			Get card's value
			*			IF an ace is drawn
			*				Card's soft value is 11
			*				Set ace flag to true
			*			END IF
			*			Update dealer's total
			*			Set button icon to unknown
			*			Add button to array
			*		ELSE it is not the first card
			*			Create button
			*			Set button icon to unknown
			*			Add button to array
			*			Add dealer card to panel
			*		END IF
			*	END FOR
			*END DealerPanel
			*
			************************************************/

			public DealerPanel()
			{
				//local constants
				final int MAX_CARDS = 6;

				//local variables
				dealerCardArray = new JButton[MAX_CARDS];
				int cardValue;


				/*******************/

				//Set background to green
				setBackground(Color.white);

				//Create 6 buttons that represent cards
				for (int i = 0; i < MAX_CARDS; i++)
				{
					//IF the first card is being created
					if (i == 0)
					{
						//Create button
						dealerCard = new JButton();

						//Add to panel
						add(dealerCard);

						//Get a card from the deck
						myCard = deck.addToHand();

						//Get current card's image
						cardImage = myCard.getImage();

						//Get card's value
						cardValue = myCard.getFaceValNum();

						//IF an ace is drawn
						if (cardValue == 1)
						{
							//Card's soft value is 11
							cardValue = 11;

							//Set ace flag to true
							aceDetectedDealer = true;

						}//END IF

						//Update dealer's total
						dealerTotal += cardValue;

						//Set button icon to unknown
						dealerCard.setIcon(cardImage);

						//Add button to array
						dealerCardArray[i] = dealerCard;

					}

					//ELSE it is not the first card
					else
					{

						//Create button
						dealerCard = new JButton();

						//Set button icon to unknown
						dealerCard.setIcon(UNKNOWN);

						//Add button to array
						dealerCardArray[i] = dealerCard;

						//Add dealer card to panel
						add(dealerCard);

					}//END IF

				}//END FOR

			}//END DealerPanel constructor

		}//END DealerPanel class

		private class PlayerPanel extends JPanel
		{
			/*********************************************
			*Title: 	   PlayerPanel
			*Author: 	   Kevin Morales
			*Date Due:     15 May 2019
			*Course: 	   CSC 264-501
			*Description:  This class deals with the player's actions during the game.
			*
			*Refined Algorithm:
			*BEGIN GamePanel
			*	Set background to green
			*	Arrange buttons in horizontal row
			*	Create 6 buttons that represent cards
			*		IF first  or second buttons are being created
			*			Create first button
			*			Add to array
			*			Get a card from the deck
			*			Get current card's image
			*			Get card's value
			*			IF an ace is drawn
			*				Card's soft value is 11
			*				Set ace flag to true
			*			END IF
			*			Update player's total
			*			Reveal card by setting button to image
			*			IF player gets 21 on drawn cards
			*				Let user know they won
			*				Reveal ready and quit buttons
			*				Give user 1.5 times their bet back
			*				Update money label
			*				newTurn();
			*			END IF
			*			add Button to panel
			*		ELSE cards 3 through 6 are being made
			*			Create button
			*			Set icon to question mark
			*			Give button action Listener
			*			add Button to array of buttons
			*			add Button to panel
			*			Disable button
			*		END IF
			*	END FOR
			*END PlayerPanel constructor
			*
			**********************************************/
			public PlayerPanel()
			{
				//local constants
				final int MAX_CARDS = 6;

				//local variables
				playerCardArray = new JButton[6];
				int cardValue;

				/*******************/

				//Set background to green
				setBackground(Color.white);

				//Arrange buttons in horizontal row
				setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

				//Create 6 buttons that represent cards
				for (int i = 0; i < MAX_CARDS; i++)
				{
					//IF first  or second buttons are being created
					if (i == 0 || i == 1)
					{
						//Create first button
						playerCard = new JButton();

						//Add to array
						playerCardArray[i] = playerCard;

						//Get a card from the deck
						myCard = deck.addToHand();

						//Get current card's image
						cardImage = myCard.getImage();

						//Get card's value
						cardValue = myCard.getFaceValNum();

						//IF an ace is drawn
						if (cardValue == 1)
						{
							//Card's soft value is 11
							cardValue = 11;

							//Set ace flag to true
							aceDetected = true;

						}//END IF

						//Update player's total
						playerTotal += cardValue;

						//Reveal card by setting button to image
						playerCardArray[i].setIcon(cardImage);

						//IF player gets 21 on drawn cards
						if (playerTotal == 21)
						{
							//Let user know they won
							score.setText("YOU WIN: Dealer: " + dealerTotal + "     You: " + playerTotal);

							//Reveal ready and quit buttons
							readyForNext.setVisible(true);
							quit.setVisible(true);

							//Give user 1.5 times their bet back
							totalMoney += ((betAmount * 2) + (betAmount / 2));

							//Update money label
							myMoney.setText("My Money: $" + totalMoney);

							//newTurn();

						}//END IF

						//add Button to panel
						add(playerCard);

					}

					//ELSE cards 3 through 6 are being made
					else
					{

						//Create button
						playerCard = new JButton();

						//Set icon to question mark
						playerCard.setIcon(UNKNOWN);

						//Give button action Listener
						playerCard.addActionListener(new PlayerListener());

						//add Button to array of buttons
						playerCardArray[i] = playerCard;

						//add Button to panel
						add(playerCard);

						//Disable button
						playerCard.setEnabled (false);

					}//END IF

				}//END FOR

			}//END PlayerPanel constructor

			private class PlayerListener implements ActionListener
			{

				/*********************************************
				*Title: 	   PlayerPanel
				*Author: 	   Kevin Morales
				*Date Due:     15 May 2019
				*Course: 	   CSC 264-501
				*Description:  This class deals with the player's actions during the game.
				*
				*Refined Algorithm:
				*BEGIN GamePanel
				*	Get a card from the deck
				*	Get current card's image
				*	Get card's value
				*	Update player's total
				*	Reveal card by setting button to image
				*	Update score label
				*	IF dealer's total is less than 18
				*		Reveal a dealer card
				*	END IF
				*	FOR each element in the array
				*		Disable the card
				*	END FOR
				*END PlayerListener
				*
				****************************************************/

				public void actionPerformed(ActionEvent event)
				{
					//local constants

					//local variables
					Object source = event.getSource();
					int cardValue;

					/**************************/

					//Get a card from the deck
					myCard = deck.addToHand();

					//Get current card's image
					cardImage = myCard.getImage();

					//Get card's value
					cardValue = myCard.getFaceValNum();

					//Update player's total
					playerTotal += cardValue;

					//Reveal card by setting button to image
					playerCardArray[2].setIcon(cardImage);

					//Update score label
					updateScore();

					//IF dealer's total is less than 18
					if (dealerTotal < 18)

						//Reveal a dealer card
						revealCard();

					//END IF

					//FOR each element in the array
					for (int i = 0; i < playerCardArray.length; i++)

						//Disable the card
						playerCardArray[i].setEnabled(false);

					//END FOR

				}//END PlayerListener constructor

			}//END PlayerListener class

		}//END PlayerPanel class

		private class CenterPanel extends JPanel
		{
			/*********************************************
			*Title: 	   CenterPanel
			*Author: 	   Kevin Morales
			*Date Due:     15 May 2019
			*Course: 	   CSC 264-501
			*Description:  This class contains the score and result of the game
			*
			*Refined Algorithm:
			*BEGIN CenterPanel
			*	Set background color to green
			*	Set up a border layout
			*	Set score font to 40
			*	Add score label
			*	Add button label
			*END CenterPanel
			*
			**********************************************/

			public CenterPanel()
			{
				//local constants

				//local variables
				score = new JLabel("Dealer: " + dealerTotal + "     You: " + playerTotal);

				readyForNext = new JButton("Ready For New Hand");
				quit = new JButton("I'm done");
				playAgain = new JButton("Play new Game");
				ButtonPanel buttons = new ButtonPanel();

				/*********************/

				//Set background color to green
				setBackground(Color.green);

				//Set up a border layout
				setLayout(new BorderLayout());

				//Set score font to 40
				score.setFont(new Font("Serif", Font.PLAIN, 40));

				//Add score label
				add(score, BorderLayout.NORTH);

				//Add button label
				add(buttons, BorderLayout.SOUTH);

			}//END CenterPanel constructor

			private class ButtonPanel extends JPanel
			{
				/*********************************************
				*Title: 	   ButtonPanel
				*Author: 	   Kevin Morales
				*Date Due:     15 May 2019
				*Course: 	   CSC 264-501
				*Description:  This class has the buttons that move the game along
				*			   after the end of a turn or a game.
				*
				*Refined Algorithm:
				*BEGIN ButtonPanel
				*	Create ready button
				*	Create quit button
				*	Create play again button
				*	Set the background to green
				*	Add ready button
				*	Add the quit button
				*	Add the play again button
				*END ButtonPanel constructor
				*********************************************/

				public ButtonPanel()
				{
					//local constants

					//local variables

					/*****************/

					//Create ready button
					readyForNext.addActionListener(new ButtonListener());
					readyForNext.setVisible(false);
					readyForNext.setFont(new Font("Serif", Font.PLAIN, 40));

					//Create quit button
					quit.addActionListener(new ButtonListener());
					quit.setVisible(false);
					quit.setFont(new Font("Serif", Font.PLAIN, 40));

					//Create play again button
					playAgain.addActionListener(new ButtonListener());
					playAgain.setVisible(false);
					playAgain.setFont(new Font("Serif", Font.PLAIN, 40));

					//Set the background to green
					setBackground(Color.green);

					//Add ready button
					add(readyForNext);

					//Add the quit button
					add(quit);

					//Add the play again button
					add(playAgain);

				}//END ButtonPanel constructor

				private class ButtonListener implements ActionListener
				{
					/*********************************************
					*Title: 	   ButtonListener
					*Author: 	   Kevin Morales
					*Date Due:     15 May 2019
					*Course: 	   CSC 264-501
					*Description:  This class deals with the actions after a loss or win of
					*			   a game or hand.
					*
					*Refined Algorithm:
					*BEGIN ButtonPanel
					*	IF user wants to continue playing
					*		Start a new turn
					*	ELSE IF user wants to quit
					*		end the game
					*	ELSE user wants to play a new game
					*		Call method to start new game
					*	END IF
					*END ButtonListener
					*
					*******************************************/

					public void actionPerformed(ActionEvent event)
					{
						//local constants

						//local variables
						Object source = event.getSource();

						/**************************/

						//IF user wants to continue playing
						if (source == readyForNext)

							//Start a new turn
							newTurn();

						//ELSE IF user wants to quit
						else if (source == quit)
						{
							//end the game
							endTheGame();

						}

						//ELSE user wants to play a new game
						else if (source == playAgain)

							//Call method to start new game
							newGame();

						//END IF

					}//END actionPerformed

				}//END ButtonListener

			}//END ButtonPanel class

		}//END CenterPanel class

	}//END GamePanel class

	private class EndPanel extends JPanel
	{
		/*********************************************
		*Title: 	   EndPanel
		*Author: 	   Kevin Morales
		*Date Due:     15 May 2019
		*Course: 	   CSC 264-501
		*Description:  This class deals with the end of the game
		*
		*Refined Algorithm:
		*BEGIN EndPanel
		*	setBackground to white
		*	set up end label
		*	add end label
		*END EndPanel
		*
		*************************************/

		public EndPanel()
		{
			//local constants

			//local variables


			/********************/

			//setBackground to white
			setBackground(Color.white);

			//set up end label
			endLabel.setFont(new Font("Serif", Font.PLAIN, 40));

			//add end label
			add(endLabel);

		}//END constructor

	}//END EndPanel

	/*********************************************
	*Title: 	   revealCard
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This method reveals dealer cards
	*
	*Refined Algorithm:
	*BEGIN revealCard
	*	IF dealerTotal is less than 17
	*		Get a card from the deck
	*		Get current card's image
	*		Get card's value
	*		Update player's total
	*		Reveal card by setting button to image
	*		Increment revealed card count
	*	END IF
	*	Update score label
	*END revealCard
	*
	*****************************************************/

	public void revealCard()
	{
		//local constants

		//local variables
		int cardValue;

		/**********************/

		//IF dealerTotal is less than 17
		if(dealerTotal < 17)
		{
			//Get a card from the deck
			myCard = deck.addToHand();

			//Get current card's image
			cardImage = myCard.getImage();

			//Get card's value
			cardValue = myCard.getFaceValNum();

			//Update player's total
			dealerTotal += cardValue;

			//Reveal card by setting button to image
			dealerCardArray[revealCount].setIcon(cardImage);

			//Increment revealed card count
			revealCount++;

		}//END IF

		//Update score label
		updateScore();

	}//END revealCard


	/*********************************************
	*Title: 	   updateScore
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This method reveals dealer cards
	*
	*Refined Algorithm:
	*BEGIN updateScore
		IF playerTotal is greater than 21 AND there is an ace
			The ace is now valued at 1 and the player's total is subtracted by 10
			Update score label
		ELSE IF dealer's total is greater than 21 and there is an ace
			The ace is now valued at 1 and the dealer's total is subtracted by 10
			Update score label
		ELSE IF player total is greater than 21
			Let user know they lost and show score
			IF user is out of money
				Reveal play again button
			ELSE user still has money
				Reveal ready button
			END IF
			Reveal quit button
		ELSE IF dealer total is greater than 21
			Let user know they won and show the score
			Reveal ready and quit buttons
			Give user their earnings
			Update money label
		IF player gets 21
			Let user know they won
			Reveal ready and quit buttons
			Give user their earnings
			Update money label
			IF dealer did not bust AND player beat dealer
				END FOR
				Update score
				Reveal ready and quit buttons
				Give user double their bet back
				Update money label
			ELSE IF dealer did not bust AND dealer beat player
				END FOR
				Update score
				IF user is out of money
					Reveal play again button
				ELSE user still has money
					Reveal ready button
				END IF
				Reveal quit button
				Update money label
			IF it's a tie and the user decides to stay
				END FOR
				Update score
			END IF
			IF user is out of money
				Reveal play again button
			ELSE user still has money
				Reveal ready button
			END IF
			Reveal quit button
			Update money label
		ELSE more turns are needed to decide a winner
			update score label
		END IF
	END updateScore

	**********************************************************/

	public void updateScore()
	{
		//local constants

		//local variables

		/**********************/

		//IF playerTotal is greater than 21 AND there is an ace
		if ( playerTotal > 21 && aceDetected == true)
		{
			//The ace is now valued at 1 and the player's total is subtracted by 10
			playerTotal -= 10;

			//Update score label
			score.setText("Dealer: " + dealerTotal + "     You: " + playerTotal + "  Saved by an Ace!");

		}

		//ELSE IF dealer's total is greater than 21 and there is an ace
		else if (dealerTotal > 21 && aceDetectedDealer == true)
		{
			System.out.println(" else if (dealerTotal > 21 && aceDetectedDealer == true)");
			//The ace is now valued at 1 and the dealer's total is subtracted by 10
			dealerTotal -= 11;

			//Update score label
			score.setText("Dealer: " + dealerTotal + "     You: " + playerTotal);

		}

		//ELSE IF player total is greater than 21
		else if (playerTotal > 21)
		{

			System.out.println("else if(playerTotal > 21)");
			//Let user know they lost and show score
			score.setText("YOU LOSE: Dealer: " + dealerTotal + "     You: " + playerTotal);

			//IF user is out of money
			if (totalMoney == 0)
			{
				//Reveal play again button
				playAgain.setVisible(true);

			}

			//ELSE user still has money
			else
			{

				//Reveal ready button
				readyForNext.setVisible(true);

			}//END IF

			//Reveal quit button
			quit.setVisible(true);

		}

		//ELSE IF dealer total is greater than 21
		else if (dealerTotal > 21)
		{
			System.out.println("else if (dealerTotal > 21)");

			//Let user know they won and show the score
			score.setText("YOU WIN: Dealer: " + dealerTotal + "     You: " + playerTotal);

			//Reveal ready and quit buttons
			readyForNext.setVisible(true);
			quit.setVisible(true);

			//Give user their earnings
			totalMoney += (betAmount * 2);

			//Update money label
			myMoney.setText("My Money: $" + totalMoney);
		}

		//IF player gets 21
		else if(playerTotal == 21)
		{
			System.out.println("else if(playerTotal == 21)");

			//Let user know they won
			score.setText("YOU WIN: Dealer: " + dealerTotal + "     You: " + playerTotal);

			//Reveal ready and quit buttons
			readyForNext.setVisible(true);
			quit.setVisible(true);

			//Give user their earnings
			totalMoney += ((betAmount * 2) + (betAmount / 2));

			//Update money label
			myMoney.setText("My Money: $" + totalMoney);
		}

		else if(stayDetected = true && dealerTotal < 21 && dealerTotal >= 18)
		{
			System.out.println("else if(dealerTotal < 21 && dealerTotal >= 18)");

			//IF dealer did not bust AND player beat dealer
			if (dealerTotal < playerTotal)
			{
				System.out.println("if (dealerTotal > playerTotal)");

				for (int i = 0; i < playerCardArray.length; i++)

					playerCardArray[i].setEnabled(false);

				//END FOR

				//Update score
				score.setText("YOU WIN: Dealer: " + dealerTotal + "     You: " + playerTotal);

				//Reveal ready and quit buttons
				readyForNext.setVisible(true);
				quit.setVisible(true);

				//Give user double their bet back
				totalMoney += (betAmount * 2);

				//Update money label
				myMoney.setText("My Money: $" + totalMoney);

			}

			//ELSE IF dealer did not bust AND dealer beat player
			else if (dealerTotal > playerTotal)
			{
				System.out.println("else if (dealerTotal > playerTotal)");
				for(int i = 0; i < playerCardArray.length; i++)

					playerCardArray[i].setEnabled(false);

				//END FOR

				//Update score
				score.setText("YOU LOSE: Dealer: " + dealerTotal + "     You: " + playerTotal);

				//IF user is out of money
				if (totalMoney == 0)

					//Reveal play again button
					playAgain.setVisible(true);

				//ELSE user still has money
				else

					//Reveal ready button
					readyForNext.setVisible(true);


				//END IF

				//Reveal quit button
				quit.setVisible(true);

				//Update money label
				myMoney.setText("My Money: $" + totalMoney);

			}

			//ELSE IF it's a tie and the user decides to stay
			else if(playerTotal == dealerTotal && stayDetected == true)
			{
				System.out.println("else if(playerTotal == dealerTotal && stayDetected == true)");
				for(int i = 0; i < playerCardArray.length; i++)

					playerCardArray[i].setEnabled(false);

				//END FOR

				//Update score
				score.setText("IT'S A TIE! NO MONEY LOST OR EARNED: Dealer: " + dealerTotal + "     You: " + playerTotal);
				readyForNext.setVisible(true);
				quit.setVisible(true);
				totalMoney += betAmount;
				myMoney.setText("My Money: $" + totalMoney);

			}//END IF

		}

		else if(playerTotal == dealerTotal && stayDetected == false)
		{
			//update score label
			score.setText("Dealer: " + dealerTotal + "     You: " + playerTotal);

		}

		else if (dealerTotal == 21)
		{
			score.setText("YOU LOSE: Dealer: " + dealerTotal + "     You: " + playerTotal);

			//IF user is out of money
			if (totalMoney == 0)

				//Reveal play again button
				playAgain.setVisible(true);

			//ELSE user still has money
			else

				//Reveal ready button
				readyForNext.setVisible(true);

			//END IF

			//Reveal quit button
			quit.setVisible(true);

			//Update money label
			myMoney.setText("My Money: $" + totalMoney);

		}

		//ELSE more turns are needed to decide a winner
		else

			//update score label
			score.setText("Dealer: " + dealerTotal + "     You: " + playerTotal);

		//END IF

	}//END updateScore

	/*********************************************
	*Title: 	   newTurn
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This method starts a new turn within a game
	*
	*Refined Algorithm:
	*BEGIN updateScore
		Reset player and dealer totals
		Enable stay button
		Hide quit and ready buttons
		Reset Ace Detectors to false
		Reset cards to initial positions
			IF first  or second buttons are being created
				Get a card from the deck
				Get current card's image
				Get card's value
				IF an ace is drawn
					Card's soft value is 11
				END IF
				Update player's total
				Reveal card by setting button to image
				IF player gets 21 on first try
					Let user know they won
					Activate ready and quit buttons
					Give user their earnings
					Call method to begin new turn
				END IF
			ELSE the rest of the cards are being created
				Set the icon to a question mark
			END IF
		END FOR
		FOR each element in the array
			IF the first element is the current one
				Shuffle deck
				Get a card from the deck
				Get current card's image
				Set dealer card image to drawn card
				Set card's value
				IF an ace is drawn
					Card's soft value is 11
					Set ace detector to true
				Update dealer's total
				Set button icon to card drawn
			ELSE card is not the first
				Set icon to unknown card
			END IF
		END FOR
		Enable and disable appropriate buttons in player card array
			IF it is the first or the second card
				enable buttons
			ELSE it is not the first or second card
				disable buttons
			END IF
		END FOR
		Reset betting buttons
		END FOR
		Reset betting totals
		Reset reveal count
		reset stay flag to false
		update score
	END newTurn

	****************************************************/

	public void newTurn()
	{
		//local constants

		//local variables
		int cardValue;

		/**********************/

		//Reset player and dealer totals
		playerTotal = 0;
		dealerTotal = 0;

		//Enable stay button
		stay.setEnabled(true);

		//Hide quit and ready buttons
		quit.setVisible(false);
		readyForNext.setVisible(false);
		playAgain.setVisible(false);

		//Reset Ace Detectors to false
		aceDetected = false;
		aceDetectedDealer = false;

		//Reset cards to initial positions
		for (int i = 0; i < playerCardArray.length; i++)
		{

			//IF first  or second buttons are being created
			if (i == 0 || i == 1)
			{

				//Get a card from the deck
				myCard = deck.addToHand();

				//Get current card's image
				cardImage = myCard.getImage();

				//Get card's value
				cardValue = myCard.getFaceValNum();

				//IF an ace is drawn
				if (cardValue == 1)
				{
					//Card's soft value is 11
					cardValue = 11;

					aceDetected = true;

				}//END IF

				//Update player's total
				playerTotal += cardValue;

				//Reveal card by setting button to image
				playerCardArray[i].setIcon(cardImage);

				//IF player gets 21 on first try
				if (playerTotal == 21)
				{
					//Let user know they won
					score.setText("YOU WIN: Dealer: " + dealerTotal + "     You: " + playerTotal);

					//Activate ready and quit buttons
					readyForNext.setVisible(true);
					quit.setVisible(true);

					//Give user their earnings
					totalMoney += ((betAmount * 2) + (betAmount / 2));
					myMoney.setText("My Money: $" + totalMoney);

					//Call method to begin new turn
					newTurn();

				}//END IF

			}

			//ELSE the rest of the cards are being created
			else

				//Set the icon to a question mark
				playerCardArray[i].setIcon(UNKNOWN);

			//END IF

		}//END FOR

		//FOR each element in the array
		for(int i = 0; i < dealerCardArray.length; i++)
		{
			//IF the first element is the current one
			if (i == 0)
			{
				//Shuffle deck
				deck.shuffle();

				//Get a card from the deck
				myCard = deck.addToHand();

				//Get current card's image
				cardImage = myCard.getImage();

				//Set dealer card image to drawn card
				dealerCardArray[i].setIcon(cardImage);

				//Get card's value
				cardValue = myCard.getFaceValNum();

				//IF an ace is drawn
				if (cardValue == 1)
				{
					//Card's soft value is 11
					cardValue = 11;

					//Set ace detector to true
					aceDetectedDealer = true;

				}

				//Update dealer's total
				dealerTotal = cardValue;

				//Set button icon to card drawn
				dealerCard.setIcon(cardImage);

			}

			//ELSE card is not the first
			else

				//Set icon to unknown card
				dealerCardArray[i].setIcon(UNKNOWN);

			//END IF

		}//END FOR

		//Enable and disable appropriate buttons in player card array
		for (int i = 0; i < playerCardArray.length; i++)
		{
			//IF it is the first or the second card
			if (i == 0 || i == 1)

				//enable buttons
				playerCardArray[i].setEnabled(true);

			//ELSE it is not the first or second card
			else

				//disable buttons
				playerCardArray[i].setEnabled(false);

			//END IF

		}//END FOR

		//Reset betting buttons
		for (int i = 0; i < bettingButtons.length; i++)

			bettingButtons[i].setEnabled(true);

		//END FOR

		//Reset betting totals
		betLabel.setText("BET: $0");
		totalBet = 0;

		//Reset reveal count
		revealCount = 1;

		//reset stay flag to false
		stayDetected = false;

		//update score
		score.setText("Dealer: " + dealerTotal + "     You: " + playerTotal);

	}//END newTurn

	/*********************************************
	*Title: 	   newGame
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This method starts a new turn within a game
	*
	*Refined Algorithm:
	*BEGIN newGame
	*	Reset player money
	*	Call new turn method
	*END newGame
	***************************************/

	public void newGame()
	{
		//local constants

		//local variables
		int cardValue;

		/******************/

		//Reset player money
		totalMoney = INITIAL_PURSE;
		myMoney.setText("My Money: $" + totalMoney);

		//Call new turn method
		newTurn();

	}//END newGame

	/*********************************************
	*Title: 	   endTheGame
	*Author: 	   Kevin Morales
	*Date Due:     15 May 2019
	*Course: 	   CSC 264-501
	*Description:  This method starts a new turn within a game
	*
	*Refined Algorithm:
	*BEGIN endTheGame
	*	Reveal end game screen
	*	Display thank you message
	*END endTheGame
	***************************************/

	public void endTheGame()
	{
		//local constants
		final String THANK_YOU = "Thanks for playing!!!";
		//local variables


		/*******************/

		//Reveal end game screen
		myGame.setVisible(false);

		//Set up end window
		frame2 = new JFrame(THANK_YOU);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.getContentPane().add(new EndGamePanel());
		frame2.pack();
		frame2.setVisible(true);
		frame2.setBackground(Color.black);

	}//END endTheGame

}//END GameWindow class