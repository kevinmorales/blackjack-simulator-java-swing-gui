import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;
import java.util.Scanner;
import  java.io.*;

public class EndGamePanel extends JPanel
{
	//class constants

	//class variables
	private CreditsPanel myCredits;
	private int x, y, moveY;
	private ImageIcon credits;
	private Timer timer;
 	Long currentFrame;



    String status;
    static String filePath;

	/*****************************/

	public EndGamePanel()
	{
		//local constants

		//local variables


		/***************************/

		//Set background to black
		setBackground(Color.black);

		//Instantiate credits panel
		myCredits = new CreditsPanel();

		//Add credits to panel
		add(myCredits);

		//Set size of panel
		setPreferredSize(new Dimension(800, 500));

	}

	private class CreditsPanel extends JPanel
	{
		//class constants
		private final int DELAY = 50;	//time of delay constant

		//class variables

		public CreditsPanel()
		{
			//local constants

			//local variables

			/*****************************/

			//Set credits to credits gif
			credits = new ImageIcon("Credits//EndCredits.gif");

			//Instantiate timer
			timer = new Timer(DELAY, new CreditsListener());

			//Set up begining x and y coordinates
			x = 0;
			y = 500;
			moveY = 3;

			//Start timer
			timer.start();

			//Set size and color of window
			setPreferredSize(new Dimension(600, 500));
			setBackground(Color.black);

		}

		public void paintComponent(Graphics page)
		{

			super.paintComponent(page);
			credits.paintIcon(this, page, x, y);

		}//END paintComponent



		private class CreditsListener implements ActionListener
  	 	{

			public void actionPerformed(ActionEvent event)
			{
				//local constants

				//local variables


				/***********************/

				//Scroll up by 3
				y -= moveY;

				//Repaint panel
				repaint();

			 }//END actionPerformed

		 }//END CreditsListener

	}//END TankPanel

}//END MainPanel
