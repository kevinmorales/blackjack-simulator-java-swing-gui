/***************************************************************************
*Title: 	  DeckOfCards
*Author: 	  Kevin Morales
*Date:  	  28 March 2019
*Course:
*Description: This class will serve a single deck of card objects. This class
*			  does not require instance data to create a new deck.
*
***************************************************************************/

import java.util.*;
import javax.swing.*;

public class DeckOfCards
{
	//class constants
	private final int NUM_FACES = 13;


	//class variables
	private Card head;		//points to first card
	private Card first;		//temp position for first card
	private Card current;	//card currently being processed
	private Card prev;		//card previously processed
	private Card next;		//Next card in list

	private int totalCards = 52;
	private int cardID;
	private int faceCounter;
	private int cardCounter;
	private String stringFace;
	private ImageIcon image;


	/***********************************************************************
	*Title: 		DeckOfCards
	*Author: 		Kevin Morales
	*Description: 	This is the constuctor. This method will initiate the head
	*			  	node to null.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN DeckOfCards
	*		Inititiate head to null
	*	END DeckOfCards
	*
    *************************************************************************/

	public DeckOfCards()
	{
		//local constants

		//local variables

		/***************************/

		//Inititiate head to null
		head = null;

		//Create new deck upon invoking constructor
		createDeck();

	}//END constructor

	/***********************************************************************
	*Title: 		createDeck
	*Author: 		Kevin Morales
	*Description: 	This method will create a new deck. This will be done
	*				by creating 52 unique card objects and linking them
	*				in a list.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN createDeck
	*		set cardID = 1
	*		FOR suitCounter = 1 TO numberOfSuits
	*			IF suitCounter = 1
	*				Suit is Hearts
	*			ELSE IF suitCounter = 2
	*				Suit is Clubs
	*			ELSE IF suitCounter = 3
	*				Suit is Spades
	*			ELSE
	*				Suit is Diamsons
	*			END IF
	*			FOR faceCounter = 1 to numberOfFaces
	*				IF faceCounter = 1
	*					Face is One
	*				ELSE IF faceCounter = 2
	*					Face is Two
	*				ELSE IF faceCounter = 3
	*					Face is Three
	*				ELSE IF faceCounter = 4
	*					Face is four
	*				ELSE IF faceCounter = 5
	*					Face is five
	*				ELSE IF faceCounter = 6
	*					Face is six
	*				ELSE IF faceCounter = 7
	*					Face is seven
	*				ELSE IF faceCounter = 8
	*					Face is eight
	*				ELSE IF faceCounter = 9
	*					Face is nine
	*				ELSE IF faceCounter = 10
	*					Face is 10
	*				ELSE IF faceCounter = 11
	*					Face is Jack
	*				ELSE IF faceCounter = 12
	*					Face is Queen
	*				ELSE faceCounter = 13
	*					Face is King
	*				END IF
	*				Insert Card into deck
	*			END FOR
	*		END FOR
	*		Increment card ID
	*	END createDeck
	*
    *************************************************************************/

	public void createDeck()
	{
		//local constants

		//local variables

		/*************************/
		//set cardID = 1
		cardID = 1;

		//FOR
		for(int counter = 0; counter < 4; counter++)
		{

			//FOR (faceCounter = 1 to numberOfFaces)
			for (faceCounter = 1; faceCounter < (NUM_FACES + 1); faceCounter++)
			{

				//IF faceCounter = 1
				if (faceCounter == 1)
				{
					//Face is One
					stringFace = "Ace";

					//Set Icon
					image = new ImageIcon("CardImages//aceCard.png");

				}

				//ELSE IF faceCounter = 2
				else if (faceCounter == 2)
				{
					//Face is Two
					stringFace = "Two";

					//Set Icon
					image = new ImageIcon("CardImages//2card.png");

				}

				//ELSE IF faceCounter = 3
				else if (faceCounter == 3)
				{
					//Face is Three
					stringFace = "Three";

					//Set Icon
					image = new ImageIcon("CardImages//3card.png");

				}

				//ELSE IF faceCounter = 4
				else if (faceCounter == 4)
				{

					//Face is four
					stringFace = "Four";

					//Set Icon
					image = new ImageIcon("CardImages//4card.png");

				}

				//ELSE IF faceCounter = 5
				else if (faceCounter == 5)
				{

					//Face is five
					stringFace = "Five";

					//Set Icon
					image = new ImageIcon("CardImages//5card.png");

				}

				//ELSE IF faceCounter = 6
				else if (faceCounter == 6)
				{

					//Face is six
					stringFace = "Six";

					//Set Icon
					image = new ImageIcon("CardImages//6card.png");

				}

				//ELSE IF faceCounter = 7
				else if (faceCounter == 7)
				{

					//Face is seven
					stringFace = "Seven";

					//Set Icon
					image = new ImageIcon("CardImages//7card.png");

				}

				//ELSE IF faceCounter = 8
				else if (faceCounter == 8)
				{

					//Face is eight
					stringFace = "Eight";

					//Set Icon
					image = new ImageIcon("CardImages//8card.png");

				}

				//ELSE IF faceCounter = 9
				else if (faceCounter == 9)
				{

					//Face is nine
					stringFace = "Nine";

					//Set Icon
					image = new ImageIcon("CardImages//9card.png");

				}

				//ELSE IF faceCounter = 10
				else if (faceCounter == 10)
				{

					//Face is 10
					stringFace = "Ten";

					//Set Icon
					image = new ImageIcon("CardImages//10card.png");

				}

				//ELSE IF faceCounter = 11
				else if (faceCounter == 11)
				{

					//Face is Jack
					stringFace = "Jack";

					//Set Icon
					image = new ImageIcon("CardImages//JackCard.png");

				}

				//ELSE IF faceCounter = 12
				else if (faceCounter == 12)
				{

					//Face is Queen
					stringFace = "Queen";

					//Set Icon
					image = new ImageIcon("CardImages//QueenCard.png");

				}

				//ELSE faceCounter = 13
				else
				{

					//Face is King
					stringFace = "King";

					//Set Icon
					image = new ImageIcon("CardImages//KingCard.png");

				}//END IF

				//Call method to create card
				createCard();

			}//END FOR

		}//END FOR

		//Increment card ID
		cardID++;

	}//END createDeck

	public void createCard()
	{
		//local constants

		//local variables
		Card newCard = new Card(); //Instantiate new card object
		int value;

		/**********************************/


		if (faceCounter == 11 || faceCounter == 12 || faceCounter == 13)

			value = 10;

		else

			value = faceCounter;

		//END IF

		//Create a new card
		newCard.setFaceValue(value);
		newCard.setFaceStr(stringFace);
		newCard.setID(cardID);
		newCard.setImage(image);

		//Insert card into deck
		insert(newCard);

	}//END createCard

	/***********************************************************************
	*Title		: insert
	*Author		: Kevin Morales
	*Description: This method will insert a card into the deck.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN insert
	*		Create a new card
	*		IF there are no cards in list
	*			Make the new card the first card
	*		ELSE there are cards in deck
	*			Move first card to previous holder
	*			Set next card as the current card
	*			WHILE current card is not null
	*				Move to next card
	*			END WHILE
	*			Place new card in the last spot in the list
	*		END IF
	*		Update head of list
	*	END insert
	*
    *************************************************************************/

	public void insert(Card newCard)
	{
		//local constants

		//local variables

		first = head;  //store head of list

		/******************************************************/

		//IF there are no cards in list
		if (areThereCards() == false)
		{
			//Make the new card the first card
			first = newCard;

		}
		//ELSE there are cards in deck
		else
		{
			//Move first card to previous holder
			prev = first;

			//Set next card as the current card
			current = first.getNext();

			//WHILE current card is not null
			while (current != null)
			{
				//Move to next card
				prev    = current;
				current = current.getNext();

			}//END WHILE

			//place new card in the last spot in the list
			prev.setNext(newCard);

		}//END IF

		//update head of list
		head = first;

	}//END insert

	/***********************************************************************
	*Title		: areThereCards
	*Author		: Kevin Morales
	*Description: This method will check if there are cards in the deck.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN areThereCards
	*		IF there is at least one card
	*			Set boolean flag to true
	*		ELSE there are no cards left
	*			Set boolean flag to false
	*		END IF
	*		RETURN boolean flag
	*
    *************************************************************************/

	public boolean areThereCards()
	{
		//local constants

		//local variables
		boolean checker = true;
		first = head;

		/*************************/

		//If there is at least one card
		if (first != null)

			//Set boolean flag to true
			checker = true;

		//ELSE there are no cards left
		else

			//Set boolean flag to false
			checker = false;

		return checker;

	}//END areThereCards

	/***********************************************************************
	*Title		: shuffle
	*Author		: Kevin Morales
	*Description: This method will shuffle the deck of cards into a random order
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN shuffle
	*		IF deck has cards in it
	*			FOR count = 0, loop while count is less than totalCards, increment count
	*				Generate a random number
	*				FOR i = 0, loop while i is less than that number, increment i
	*					Move on to next card
	*				END FOR
	*				Call method to insert card at the end of the deck
	*			END FOR
	*		END IF
	*	END shuffle
	*
    *************************************************************************/

	public void shuffle()
	{
		//local constants

		//local variables
		first = head;
		current = first;
		int num;
		String randomCard;
		Random gen = new Random();
		Card tempCard = new Card();
		int counter = 0;

		/****************************/

		if (areThereCards() == true)
		{
			//Generate random number
			num = gen.nextInt(totalCards);

			if (num == 0)
			{
				//Get card attributes
				stringFace = first.getFaceValueStr();
				faceCounter = first.getFaceValNum();
				cardID = first.getID();
				image = first.getImage();


				//Create card with those attributes
				createCard();

				//Delete original reference to first card
				first = first.getNext();

			}

			else
			{

				//WHILE not at the end of the list
				while(current.getNext() != null && num != 0)
				{
					//Move on to next Card
					prev = current;

					//Get next card
					current = current.getNext();

					//Decrement num
					num--;

				}//END WHILE

				//IF card is found at end of list
				if ((current.getNext() == null))
				{
					//Put card in temporary holder
					tempCard = current;

					//Make the previous card the new last card
					prev.setNext (null);

					//Insert card at end of list
					insert(tempCard);

				}

				//ELSE card is found within list
				else
				{
					//Put card in temporary holder
					tempCard = current;

					//Remove card
					//Link the previous card to the next card
					prev.setNext (current.getNext());
					current.setNext (null);

					insert(tempCard);

				}//END IF

			}//END IF

		}

		//ELSE list has no more cards
		else
		{
			//Let user know there are no more cards
			System.out.println("\n\n\n");
			System.out.print("\t\t0 Cards in deck.");
			System.out.println("\n\n\n");

		}//END IF

		//update head of list
		head = first;

	}//END shuffle

	/***********************************************************************
	*Title		: addToHand
	*Author		: Kevin Morales
	*Description: This method will randomly remove a card from the deck and
	*			  return it to the driver for the hand of cards. This method
	* 			  does not need any data as a parameter.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN remove
	*		IF first position in list has a card
	*			Generate random number
	*			IF num = 0, first card is selected
	*				Set card as the drawn card
	*				Remove card
	*				Set next card as the first card
	*			ELSE check the rest of the cards
	*				WHILE not at the end of the list
	*					Move on to next Card
	*					Get next card
	*					Decrement num
	*				END WHILE
	*				IF card is found at end of list
	*					Set current card as drawn card
	*					Make the previous card the new last card
	*				ELSE card is found within list
	*					Set current card as drawn card
	*					Remove card
	*					Link the previous card to the next card
	*				END IF
	*			END IF
	*			Decrement card count
	*			Let user know how many cards are left
	*		ELSE list has no more cards
	*			Let user know there are no more cards
	*		END IF
	*		update head of list
	*		RETURN drawn card
	*	END remove
	*
    *************************************************************************/

	public Card addToHand()
	{
		//local constants

		//local variables
		int num;
		first = head;
		current = first;
		Random gen = new Random();
		Card drawnCard = new Card();

		/****************************/

		//Generate random number
		num = gen.nextInt(totalCards) + 1;

		//WHILE not at the end of the list
		while(current.getNext() != null && num != 0)
		{
			//Move on to next Card
			prev = current;

			//Get next card
			current = current.getNext();

			//Decrement num
			num--;

		}//END WHILE

		//IF card is found at end of list
		if ((current.getNext() == null))

			//Place card in holder
			drawnCard = current;


		//ELSE card is found within list
		else

			//Place card in holder
			drawnCard = current;

		//END IF

		//RETURN drawn card
		return drawnCard;

	}//END addToHand

	/***********************************************************************
	*Title		: printString
	*Author		: Kevin Morales
	*Description: This method will display a card's face and suit for the user.
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*Output Instance Data:
	*
	*Additional Instance Data or Variables
	*
	*Refined Algorithm (Pseudo-code)
	*	BEGIN printString
	*		IF there are cards in the deck
	*			Create spacing
	*			FOR(Begin at first card and loop until the next card is null)
	*				Display card
	*			END FOR
	*		ELSE there are no cards in deck
	*			Let user know there are no more cards
	*		END IF
	*	END printString
	*
    *************************************************************************/

	public void printString( ) //print out the list
	{
		//local constants

		//local variables
		first = head;  //store head of list
		int count = 0;
		/****************************/

		//IF there are cards in the deck
		if (areThereCards() == true)
		{
			//Create spacing
			System.out.println("\n\n\n");


			//Display title
			System.out.println("\t\tCards in deck:\n");
			//Begin at first card and loop until the next card is null
			for (current = first; current != null; current = current.getNext())
			{
				//Display card
				System.out.println("\t\t" + current.toString());

				count++;
			}

			//Create spacing
			System.out.println("\n\n\n");

		}



		//ELSE there are no cards in deck
		else
		{

			//Let user know there are no more cards
			System.out.println("\n\n\n");
			System.out.print("\t\tNo cards in deck.");
			System.out.println("\n\n\n");

		}//END IF

	}//END printString


}//END DeckOfCards