
import javax.swing.*;

/***************************************************************************
*Title: 	  Card
*Author: 	  Kevin Morales
*Date:  	  28 March 2019
*Course: 	  CSC 112-301W
*Description: This class serves a single card object. No data needed for
*			  instantiation.
*
***************************************************************************/
public class Card
{
	//class constants

	//class variables
	private int 	iD;
	private int 	faceValueNum;
	private String 	faceValueStr;
	private ImageIcon image;
	private Card next;				//Link to next card


	public Card()
	{
		//local constants

		//local variables

		/******************************/

		//New empty card, points to null
		next = null;

	}//END constructor Card

   /***************************************************************************
   	*Title: setNext
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*	1. New card object
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*BEGIN setNext
   	*	Set next card to the card passed through
   	*END setNext
   	*
   	***************************************************************************/

	public void setNext(Card changeNext)
    {
		//Set next card to the card passed through
		next = changeNext;

    }

   /***************************************************************************
   	*Title: getNext
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*	1. New card object
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*BEGIN getNext
   	*	return next card in list
   	*END getNext
   	*
   	***************************************************************************/

    public Card getNext()
   	{
		//RETURN next card
		return next;

   	}
   /***************************************************************************
   	*Title: getFaceValNum
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*
   	*
   	*
   	***************************************************************************/

   	public int getFaceValNum()
   	{
   		//local constants

   		//local variables

   		/************************************/

   		return faceValueNum;

   	}//END getFaceValNum


   	/***************************************************************************
   	*Title: getFaceValueStr
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*
   	*
   	***************************************************************************/

   	public String getFaceValueStr()
   	{
   		//local constants

   		//local variables

   		/************************************/

   		return faceValueStr;

   	}//END getFaceValueStr


  	/***************************************************************************
   	*Title: getID
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*
   	*
   	***************************************************************************/

   	public int getID()
   	{
   		//local constants

   		//local variables

   		/************************************/

   		return iD;

   	}//END getID

   	public ImageIcon getImage()
   	{
		//local constants

		//local variables

		/**********************/

		return image;

	}

   	public void setImage(ImageIcon picture)
   	{
		//local constants

		//local variables

		/*************************/

		image = picture;

	}


   	/***************************************************************************
   	*Title: setFaceValue
   	*Author: Kevin Morales
   	*Description:
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data:
   	*
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*
   	*
   	***************************************************************************/

   	public void setFaceValue(int newFaceVal)
   	{
   		//local constants

   		//local variables

   		/************************************/

   		//Update
   		faceValueNum = newFaceVal;

   	}//END setFaceNum

   	/***************************************************************************
   	*Title: setFaceStr
   	*Author: Kevin Morales
   	*Description:
   	*
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data: none
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*
   	*
   	***************************************************************************/

   	public void setFaceStr(String newFace)
   	{
   		//local constants

   		//local variables

   		/************************************/

   		faceValueStr = newFace;

   	}//END setFaceStr

	/***************************************************************************
	*Title: setID
	*Author: Kevin Morales
	*Description:
	*
	*Data Requirements:
	*
	*Input Instance Data:
	*	1. Updated age as an Integer
	*
	*Output Instance Data: none
	*Additional Instance Data or Variables
	*
	*Refined Algorithm
	*
	*
	***************************************************************************/

	public void setID(int newID)
	{
		//local constants

		//local variables

		/************************************/

		//Update
		iD = newID;

   	}//END setID

   	/***************************************************************************
   	*Title: toString
   	*Author: Kevin Morales
   	*Description:
   	*Data Requirements:
   	*
   	*Input Instance Data:
   	*
   	*Output Instance Data:
   	*
   	*Additional Instance Data or Variables
   	*
   	*Refined Algorithm
   	*	BEGIN toString
   	*		Format all info on one line
   	*	END toString
   	*
   	***************************************************************************/

   	public String toString()
	{
  		//local constants

  		//local variables

    	/************************************/

		return faceValueStr;

	}//END toString

}//END Card