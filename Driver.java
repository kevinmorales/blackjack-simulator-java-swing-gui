import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/***************************************************************************
 *Title: 	   Driver
 *Author: 	   Kevin Morales
 *Date Due:    15 May 2019
 *Course: 	   CSC 264-501
 *Description: This class is the driver of the program. This class will open the
 *			   game window. This program will be a modified version of blackjack.
 *
 ***************************************************************************/

public class Driver
{
	/***************************************************************************
	 *Method: 	   main
	 *Author: 	   Kevin Morales
	 *Date Due:    15 May 2019
	 *Course: 	   CSC 264-501
	 *Description: The main method will instantiate a deck of cards and a game window object.
	 *			   The main method will also send the game window object the deck of cards
	 *			   object so the game window can access it.
	 *
	 ***************************************************************************/

	public static void main (String[]args)
	{
		//local constants
		final String FRAME_TEXT = "Veintiuno!!!";

		//local variables
		DeckOfCards myDeck;
		JFrame frame;

		/*****************************/

		//Instantiate a deck of Cards
		myDeck = new DeckOfCards();

		//Set up the game window for the user
		frame = new JFrame(FRAME_TEXT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new GameWindow(myDeck));
		frame.pack();
		frame.setVisible(true);

	}//END main

}//END Driver